/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dibujarcirculo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.swing.JFrame;

/**
 *
 * @author bayer
 */
public class DibujarCirculo extends JFrame{
    
 
    public DibujarCirculo()
    {
        super ("dibujar moivimiento parabolico ");
        configurarVentana();
    }
    
    public static void main(String[] args) {
        DibujarCirculo miCirculo = new DibujarCirculo();
    }

    private void configurarVentana() {
         this.setSize(1000, 1000);
         this.setVisible(true);
         this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    /**
     *
     * @param g
     */
    @Override
    public void paint(Graphics g){
        

        
        double y=0;
        double wo=45*(3.141592/180);
        double x,Vo=100,gra=9.8,t=0.1;
        

        while(y>=0)
        {
            x=Vo*cos(wo)*t;
            y=Vo*sin(wo)*t-(1.0/2)*gra*t*t;
           
            System.out.print(x);System.out.print(",");System.out.println(y);
            super.paint(g);
            Graphics2D circulo= (Graphics2D)g;
            circulo.setStroke(new BasicStroke(10.f));
            circulo.setPaint(Color.CYAN);
            circulo.drawOval((int)x, (int)y , 10, 10);
            try{
                Thread.sleep(10*10);
            }catch(Exception e){
                System.out.println(e);
            }
            
            t=t+0.1;
 
        }
    }
}
